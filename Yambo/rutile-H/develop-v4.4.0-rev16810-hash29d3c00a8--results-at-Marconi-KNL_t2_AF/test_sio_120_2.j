#!/bin/bash -x
#SBATCH -N120 -n3840
#SBATCH --time=24:00:00           # time limits: 20 min
#SBATCH --error test_sio_120_2.err   # std-error file
#SBATCH --account=Pra18_MaX   # account number
#SBATCH --job-name=test_sio_120_2
#SBATCH --partition=knl_usr_prod        #partition to be used
#SBATCH --mem=84000MB
###SBATCH --qos=knl_qos_bprod
##SBATCH --qos=knl_qos_bprod


ntasks=3840
nthreads=1



export OMP_NUM_THREADS=$nthreads

ulimit -d unlimited
ulimit -m unlimited
ulimit -s unlimited
ulimit -v unlimited
ulimit -c 0

#
# modules
#
module purge
module load profile/base
module load profile/advanced
module load profile/candidate
module load profile/phys
module load profile/archive 

module load intel/pe-xe-2017--binary
module load intelmpi/2017--binary
module load mkl/2017--binary
module load szip/2.1--gnu--6.1.0
module load zlib/1.2.8--gnu--6.1.0
module load hdf5/1.8.17--intelmpi--2017--binary
module load env-knl

#
# env
#


MYSCRA=$CINECA_SCRATCH
MYHOME=$HOME
#
WORKDIR=$MYSCRA/TEST_YAMBO_DELIVERABLE/rutile.save

jobname=test_sio_120_2   
jobdata=test_sio_120_2   
outdir=test_sio_120_2 
IN=test120_2.in


BINDIR=$HOME/devel/yambo-devel-serial-IO/bin/
#PARA_PREFIX="mpirun -n $ntasks -ppn $ntasks_per_node"
PARA_PREFIX="mpirun -n $ntasks"


#
# checks
#
#if [ ! -d $WORKDIR ] ; then mkdir -p $WORKDIR ; fi
#
#cd $JOBDIR
#if [ -e ./SCRATCH ] ; then rm ./SCRATCH ; fi
#ln -sf $WORKDIR ./SCRATCH


#
# running
#
echo "Running GW"
cd $WORKDIR
#

mpirun $BINDIR/yambo -F $IN -J $jobname -C $outdir
#mpirun $BINDIR/p2y -v   # su un solo core
#mpirun $BINDIR/yambo
#mpirun $BINDIR/yambo -r -p p -g n -V all # per generare l'input. Senza il -V all genera un input con meno informazioni
#mpirun $BINDIR/yambo -r -p p -g n   #su un solo core

#mpirun $BINDIR/yambo -F $IN -J $jobname -C $outdir 

#mpirun $BINDIR/yambo -b -r  -V all  #genera input funzione risposta yambo BSE
#mpirun $BINDIR/yambo -o b  -y h -V all #input BSE

#mpirun $BINDIR/yambo -o b -k sex -r -b -y h -V all   #maurizia

#mpirun $BINDIR/yambo -F $IN -J "$jobname,$jobdata"
#mpirun $BINDIR/yambo -F $IN -J $jobname

#mpirun $BINDIR/ypp -q m
#mpirun $BINDIR/ypp -F ypp.in

