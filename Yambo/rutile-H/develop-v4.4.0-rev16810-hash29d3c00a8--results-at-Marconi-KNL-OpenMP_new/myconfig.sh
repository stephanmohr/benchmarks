#! /bin/bash

CINECA_PATH="/cineca/prod/opt/libraries"
MKL_PATH="/cineca/prod/opt/compilers/intel/pe-xe-2017/binary/mkl/lib/intel64"
LIBXC_PATH="$CINECA_PATH/libxc/3.0.0/intel--pe-xe-2017--binary/lib"
#
#IOTK_PATH="/marconi/prod/build/applications/qe/6.0/intelmpi--2017--binary/BA_WORK/qe-6.0/iotk/"
#IOTK_PATH="/marconi/prod/build/applications/qe/6.3_knl/intelmpi--2018--binary/BA_WORK/qe-6.3_knl/iotk"
#PETSC_PATH="$CINECA_PATH/petsc/3.7.2/intelmpi--2017--binary/"
#SLEPC_PATH="$CINECA_PATH/slepc/3.7.3/intelmpi--2017--binary/"
ETSFIO_PATH=""
#
NETCDFF_LIB="-L$CINECA_PATH/netcdff/4.4.4/intel--pe-xe-2017--binary/lib -lnetcdff"
NETCDF_LIB="-L$CINECA_PATH/netcdf/4.4.1/intel--pe-xe-2017--binary/lib -lnetcdf"
HDF5_LIB="-L$CINECA_PATH/hdf5/1.8.17/intelmpi--2017--binary/lib -libhdf5hl_fortran -lhdf5_fortran -lhdf5_hl -lhdf5"
ZLIB="-L$CINECA_PATH/zlib/1.2.8/gnu--6.1.0/lib/ -lz"
PETSC_LIB="-L$CINECA_PATH/petsc/3.7.2/intelmpi--2017--binary/"
SLEPC_LIB="-L$CINECA_PATH/slepc/3.7.3/intelmpi--2017--binary/"
NETCDF_INC="$CINECA_PATH/netcdf/4.4.1/intel--pe-xe-2017--binary/include"
NETCDFF_INC="$CINECA_PATH/netcdff/4.4.4/intel--pe-xe-2017--binary/include"
HDF5_INC="$CINECA_PATH/hdf5/1.8.17/intelmpi--2017--binary/include"

./configure \
 --enable-open-mp \
 --enable-keep-src \
 --enable-msgs-comps \
 --enable-time-profile \
 --enable-memory-profile \
 --enable-netcdf-hdf5 \
 --enable-par-linalg \
 --with-libxc-path="$LIBXC_PATH" \
 --with-iotk-path="$IOTK_PATH" \
 --with-blas-libs="-L$MKL_PATH -lmkl_core -lmkl_intel_lp64 -lmkl_sequential" \
 --with-lapack-libs="-L$MKL_PATH -lmkl_core -lmkl_intel_lp64 -lmkl_sequential" \
 --with-scalapack-libs="-L$MKL_PATH -lmkl_scalapack_lp64" \
 --with-blacs-libs="-L$MKL_PATH -lmkl_blacs_intelmpi_lp64" \
 --with-fft-libs="-mkl=sequential" \
 --with-netcdf-libs="$NETCDF_LIB" --with-netcdf-includedir="$NETCDF_INC" \
 --with-netcdff-libs="$NETCDFF_LIB" --with-netcdff-includedir="$NETCDFF_INC" \
 --with-hdf5-libs="$HDF5_LIB" --with-hdf5-includedir="$HDF5_INC"
