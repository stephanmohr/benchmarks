module purge
module load profile/base
module load profile/advanced
module load profile/candidate
module load profile/phys
module load profile/archive
module load intel/pe-xe-2017--binary
module load intelmpi/2017--binary
module load mkl/2017--binary
module load szip/2.1--gnu--6.1.0
module load zlib/1.2.8--gnu--6.1.0
module load hdf5/1.8.17--intelmpi--2017--binary
module load env-knl

