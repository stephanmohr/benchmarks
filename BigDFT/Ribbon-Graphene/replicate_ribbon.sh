nunits=(6 32 54 110 162)
cp ribbon4_in.xyz cell_in.xyz
for nunit in ${nunits[@]}
do
  ./replicate 7.88 $nunit 3 T F F
  cp cell_out.xyz ribbon_${nunit}.xyz
done
rm cell_in.xyz cell_out.xyz
