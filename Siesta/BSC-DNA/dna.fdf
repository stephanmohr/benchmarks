# FDF input file for A-DNA (11 GC base pairs) Relaxed and symmetrised.
# A. Garcia
# Based on file by Emilio Artacho 1998
#                  Improved basis. Emilio Artacho 2004

#---- USAGE COMMENTS AND TUNABLE PARAMETERS ---
#
# Use this block to change the SIZE of the system for benchmarks
# The base system has 720 atoms.
# Changing it to, for example (diagonally) 2 2 2 will use 8-times more atoms.
#
%block SuperCell
  1   0   0
  0   1   0
  0   0   1
%endblock SuperCell

# This parameter controls the fineness of the real-space grid. The higher the finer.
# It is important for performance of the routines called from dhscf, notably rhoofd
# and vmat.
# For this system a large cutoff is not really needed, but for benchmarking purposes
# it can go up to, say, 500 Ry, maybe 1000 Ry

MeshCutoff           140. Ry    

#  For benchmarks this might be enough (see below, and the general notes)
#
MaxSCFIterations      1
scf-must-converge F       # Needed to finish the program gracefully with the above line.

# -- No writing of DM in principle. Might be useful to measure also
#
WriteDM      .false.
#
#---------------------------------------------------------------
# This parameter controls the granularity of the parallel decomposition
# over orbitals (block-cyclic) and also the Scalapack blocksize
#
BlockSize 8
#---------------------------------------------------------------

#  SOLVER ------------------
SolutionMethod        diagon
#
# This option (no solver step) can be enabled in the code via a patch
SolutionMethod        dummy
#
# This option will stop the program after the setup of H (avoiding solver and forces).
#
# setup-H-only T
#---------------------------

# Basis set options -------------
# Choose one line
# 
include dna.basis.dzp.fdf             # Default "moderate" basis
# PAO.BasisSize sz                    # Minimal basis
# PAO.BasisSize tzp                   # Larger cardinality
#---------------------------------

# This option will make H more sparse by avoiding counting interactions
# among orbitals mediated only by a KB projector on a third atom.
#NeglNonOverlapInt     yes

#=========================================================
# The following can stay unchanged
#---------------------------------------------------------
SystemName         DNA
SystemLabel        DNA

use-tree-timer T   # Will produce tree-like output, plus a 'time.json' file.

# SCF options
#DM.UseSaveDM          yes
#
#
DM.MixingWeight       0.10
DM.Tolerance          1.d-3          # Could be changed to 1.d-4, etc
DM.NumberPulay        3

# Species parameters
NumberOfSpecies    5
%block ChemicalSpeciesLabel
  1   1   H
  2   6   C
  3   7   N
  4   8   O
  5  15   P
%endblock ChemicalSpeciesLabel

# Unit cell and atomic positions
LatticeConstant                   < dna.geometry.fdf
LatticeVectors                    < dna.geometry.fdf
NumberOfAtoms                     < dna.geometry.fdf
AtomicCoordinatesFormat           < dna.geometry.fdf
AtomicCoordinatesOrigin           < dna.geometry.fdf
AtomicCoordinatesAndAtomicSpecies < dna.geometry.fdf

XC.Functional         GGA
XC.Authors            PBE

# Diagon options
ElectronicTemperature  5 meV 

